# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class TimeClockPrintStart(metaclass=PoolMeta):
    __name__ = 'company.employee.time_clock.print_start'

    workplace = fields.Many2One('staff.workplace', 'Work place')


class TimeClockPrint(metaclass=PoolMeta):
    __name__ = 'company.employee.time_clock.print'

    def _get_timeclock_domain(self):
        res = super()._get_timeclock_domain()
        if self.start.workplace:
            res.append(('employee.workplace', '=', self.start.workplace.id))
        return res
