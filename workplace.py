# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from sql import Null

from trytond.model import ModelSQL, ModelView, fields
from trytond.model import Unique, DeactivableMixin
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.transaction import Transaction
from trytond import backend


class Calendar(ModelView, ModelSQL):
    """Calendar"""
    __name__ = 'staff.calendar'

    name = fields.Char('Name')
    year = fields.Integer('Year', required=True,
        states={'readonly': Eval('id', 0) > 0})
    country = fields.Many2One('country.country', 'Country')
    subdivision = fields.Many2One("country.subdivision",
            'Subdivision', domain=[('country', '=', Eval('country'))])
    city = fields.Char('City')
    days = fields.One2Many('staff.calendar.day', 'calendar', 'Days',
        order=[('date_', 'ASC')])
    rest_days = fields.MultiSelection([
        ('1', 'Monday'),
        ('2', 'Tuesday'),
        ('3', 'Wednesday'),
        ('4', 'Thursday'),
        ('5', 'Friday'),
        ('6', 'Saturday'),
        ('7', 'Sunday')], 'Rest Days', sort=False)

    @classmethod
    def __register__(cls, module_name):
        exist = backend.TableHandler.table_exist(cls._table)
        table_sql = cls.__table_handler__(module_name)
        field_exist = table_sql.column_exist('rest_days')

        super().__register__(module_name)

        table = cls.__table__()
        if exist and not field_exist:
            cursor = Transaction().connection.cursor()

            cursor.execute(*table.select(
                table.id,
                table.rest_day,
                where=(table.rest_day != Null)))
            for id_, rest_day in cursor.fetchall():
                value = cls.rest_days.sql_format([rest_day])
                cursor.execute(*table.update(
                    columns=[table.rest_days],
                    values=[value],
                    where=table.id == id_))
            table_sql.drop_column('rest_day')

    @staticmethod
    def default_rest_days():
        return ['7']

    def get_rec_name(self, name):
        return str(self.name) + ' - ' + str(self.year)

    def date_type(self, date):
        pool = Pool()
        calendarDay = pool.get('staff.calendar.day')
        if self.rest_days and date.isoweekday() in self.rest_days_values:
            return 'rest'
        res = calendarDay.search_read([
            ('calendar', '=', self),
            ('date_', '=', date)], fields_names=['day_type'], limit=1)
        if res:
            return res[0]['day_type']
        return 'work'

    @property
    def rest_days_values(self):
        return set(int(day) for day in self.rest_days)


class WorkPlace(DeactivableMixin, ModelView, ModelSQL):
    """WorkPlace"""
    __name__ = 'staff.workplace'

    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)
    calendars = fields.Many2Many('staff.workplace.calendar', 'workplace',
        'calendar', 'Calendars')
    calendar = fields.Function(fields.Many2One('staff.calendar', 'Calendar'),
        'get_calendar')

    def date_type(self, date):
        for calendar in self.calendars:
            if calendar.year == date.year:
                return calendar.date_type(date)
        return 'work'

    def get_calendar(self, name=None):
        pool = Pool()
        Date = pool.get('ir.date')
        cal = self._get_calendar(Date.today().year)
        return cal.id if cal else None

    def _get_calendar(self, year):
        for calendar in self.calendars:
            if calendar.year == year:
                return calendar
        return None


class WorkPlaceCalendar(ModelSQL):
    """WorkPlace - Calendar"""
    __name__ = 'staff.workplace.calendar'

    workplace = fields.Many2One('staff.workplace', 'WorkPlace', required=True)
    calendar = fields.Many2One('staff.calendar', 'Calendar', required=True)

    @classmethod
    def __setup__(cls):
        super(WorkPlaceCalendar, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('wp_cal_uk', Unique(t, t.workplace, t.calendar),
             'staff_workplace.msg_staff_workplace_calendar_wp_cal_uk')
        ]

    @classmethod
    def validate(cls, records):
        super(WorkPlaceCalendar, cls).validate(records)
        cls.check_validation(records)

    @classmethod
    def check_validation(cls, records):
        values = {}
        for record in records:
            values.setdefault(record.workplace.id, [])
            if record.calendar.year in values[record.workplace.id]:
                raise UserError(gettext(
                    'staff_workplace.msg_staff_workplace_calendar_unique_year',
                    workplace=record.workplace.rec_name))
            values[record.workplace.id].append(record.calendar.year)

        others = cls.search([
            ('id', 'not in', list(map(int, records))),
            ('workplace', 'in', [r.workplace.id for r in records])])
        for other in others:
            if other.calendar.year in values[other.workplace.id]:
                raise UserError(gettext(
                    'staff_workplace.msg_staff_workplace_calendar_unique_year',
                    workplace=other.workplace.rec_name))
            values[record.workplace.id].append(other.calendar.year)


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    workplace = fields.Many2One('staff.workplace', 'WorkPlace')


class CalendarDay(ModelView, ModelSQL):
    """Calendar Day"""
    __name__ = 'staff.calendar.day'

    name = fields.Char('Name')
    calendar = fields.Many2One('staff.calendar', 'Calendar', required=True,
        states={'readonly': Eval('id', 0) > 0})
    date_ = fields.Date('Date', required=True)
    day_type = fields.Selection([
        ('holiday', 'holiday'),
        ('local_holiday', 'Local Holiday'),
        ('rest', 'Rest')], 'Day Type', required=True)

    @classmethod
    def __setup__(cls):
        super(CalendarDay, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('calendar_date_uk', Unique(t, t.calendar, t.date_),
             'staff_workplace.msg_staff_calendar_day_calendar_date_uk')
        ]
        cls._order.insert(0, ('date_', 'ASC'))
