# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import datetime
from trytond.pool import Pool
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.modules.company.tests import create_company, set_company


class StaffWorkplaceTestCase(ModuleTestCase):
    """Test module"""
    module = 'staff_workplace'
    extras = ['company_time_clock']

    @with_transaction()
    def test_compute_profile_prices(self):
        """Test staff profile"""
        pool = Pool()
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        Calendar = pool.get('staff.calendar')
        CalendarDay = pool.get('staff.calendar.day')
        Workplace = pool.get('staff.workplace')

        special_days = [
            (datetime.date(2016, 1, 1), 'holiday'),
            (datetime.date(2016, 1, 6), 'local_holiday'),
            (datetime.date(2016, 3, 24), 'local_holiday'),
        ]
        test_days = [
            (datetime.date(2016, 1, 1), 'holiday'),
            (datetime.date(2016, 1, 2), 'work'),
            (datetime.date(2016, 6, 5), 'rest'),
            (datetime.date(2016, 6, 15), 'work'),
            (datetime.date(2016, 1, 6), 'local_holiday'),
            (datetime.date(2016, 9, 1), 'work'),
            (datetime.date(2016, 3, 24), 'local_holiday'),
        ]
        party = Party(name='Pam Beesly')
        party.save()
        company = create_company()
        calendar = Calendar(name='Calendar 1',
            year=2016, rest_days='7')
        calendar.save()
        workplace = Workplace(name='Workplace 1', code='WP1')
        workplace.calendars = [calendar]
        workplace.save()

        with set_company(company):
            employee = Employee(party=party.id, company=company)
            employee.workplace = workplace
            employee.save()
            for date, day_type in special_days:
                CalendarDay(
                    calendar=calendar,
                    date_=date,
                    day_type=day_type).save()
            for date, day_type in test_days:
                self.assertEqual(workplace.date_type(date), day_type)


del ModuleTestCase
